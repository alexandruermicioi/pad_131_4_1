//// D import file generated from 'src/aermicioi/broker/broker.d'
//interface Broker
//{
//	void subscribe(size_t from, size_t to);
//	void publish(size_t from, size_t to);
//	void declare(Distributor!Message distributor);
//}
//
//Broker --|> Runner
//Broker --|> Storage
//
//class BrokerImpl : Broker
//{
//		- InOutStream!RemoteCall[size_t] served;
//		- ChannelFactory!Message channelFactory_;
//		- DuplexChannelFactory!RemoteCall duplexChannelFactory_;
//		- IoScheduler scheduler_;
//			+ BrokerImpl scheduler(IoScheduler scheduler);
//			+ IoScheduler scheduler();
//			+ BrokerImpl channelFactory(ChannelFactory!Message channelFactory);
//			+ ChannelFactory!Message channelFactory();
//			+ BrokerImpl duplexChannelFactory(DuplexChannelFactory!RemoteCall duplexChannelFactory);
//			+ DuplexChannelFactory!RemoteCall duplexChannelFactory();
//		+ BrokerImpl set(size_t id, InOutStream!RemoteCall range);
//		+ BrokerImpl remove(size_t id);
//		+ void subscribe(size_t from, size_t to);
//		+ void publish(size_t from, size_t to);
//		+ void declare(Distributor!Message distributor);
//		+ void run();
//}
//
//BrokerImpl ..|> Broker
//
//interface ControlAware
//{
//	 + ControlAware control(InOutStream!RemoteCall);
//}
//
//shared interface ChannelQueue
//{
//		+ void push(E);
//		+ E pop();
//			 + shared(ChannelQueue!E) pushable(bool pushable);
//			 + bool pushable();
//			 + shared(ChannelQueue!E) popable(bool popable);
//			 + bool popable();
//		+ bool full();
//		+ bool empty();
//}
//shared class QueueImpl
//{
//		- E[size] buffer;
//		- size_t popPosition;
//		- size_t pushPosition;
//		- bool direct = true;
//		- bool pushable_ = true;
//		- bool popable_ = true;
//			 + shared(QueueImpl!(E, size)) popable(bool popable);
//			 + bool popable();
//			 + shared(QueueImpl!(E, size)) pushable(bool pushable);
//			 + bool pushable();
//		+ void push(E element);
//		+ E pop();
//		+ bool full();
//		+ bool empty();
//}
//
//QueueImpl ..|> ChannelQueue
//
//class InputChannel
//{
//		- shared ChannelQueue!E queue_;
//		- E cached;
//		- IoScheduler scheduler_;
//		- NotEmptyConditional conditional;
//		+ this();
//		+ this(shared ChannelQueue!E queue);
//			+ InputChannel!E queue(shared ChannelQueue!E queue);
//			+ InputChannel!E scheduler(IoScheduler scheduler);
//			+ IoScheduler scheduler();
//			+ shared(ChannelQueue!E) queue();
//			+ E front();
//			+ bool empty();
//			+ bool closed();
//			+ bool opened();
//		+ void close();
//		+ void open();
//		+ E moveFront();
//		+ void popFront();
//		+ int opApply(int delegate(E) dg);
//		+ int opApply(int delegate(size_t, E) dg);
//}
//
//InputChannel ..|> InputStream
//
//class OutputChannel
//{
//		- shared ChannelQueue!E queue_;
//		- IoScheduler scheduler_;
//		- NotFullConditional conditional;
//		+ this();
//		+ this(shared ChannelQueue!E queue);
//			+ OutputChannel!E queue(shared ChannelQueue!E queue)
//			+ shared(ChannelQueue!E) queue()
//			+ bool full()
//			+ OutputChannel!E scheduler(IoScheduler scheduler)
//			+ IoScheduler scheduler()
//			+ bool closed()
//			+ bool opened()
//		+ void close()
//		+ void open()
//		+ void put(E element)
//}
//
//OutputChannel ..|> OutputStream
//
//interface ChannelFactory(T)
//{
//	+ void factory();
//	 + InputStream!T input();
//	 + OutputStream!T output();
//}
//interface DuplexChannelFactory(T)
//{
//	+ void factory();
//	 + InOutStream!T start();
//	 + InOutStream!T end();
//}
//class SynchronizedQueueChannelFactory(T) : ChannelFactory!T
//{
//		- InputChannel!T input_;
//		- OutputChannel!T output_;
//		- IoScheduler scheduler_;
//			+ InputChannel!T input()
//			+ OutputChannel!T output()
//			+ SynchronizedQueueChannelFactory!T scheduler(IoScheduler scheduler)
//			+ IoScheduler scheduler()
//		+ void factory()
//}
//class DuplexChannelFactoryImpl(T) : DuplexChannelFactory!T
//{
//		- ChannelFactory!T forwardChannelFactory_;
//		- ChannelFactory!T reverseChannelFactory_;
//		- InOutStream!T start_;
//		- InOutStream!T end_;
//			+ DuplexChannelFactoryImpl!T forwardChannelFactory(ChannelFactory!T forwardChannelFactory)
//			+ ChannelFactory!T forwardChannelFactory()
//			+ DuplexChannelFactoryImpl!T reverseChannelFactory(ChannelFactory!T reverseChannelFactory)
//			+ ChannelFactory!T reverseChannelFactory()
//			+ InOutStream!T start()
//			+ InOutStream!T end()
//		+ void factory()
//}
//// D import file generated from 'src/aermicioi/broker/client.d'
//class ProducerImpl : Producer!()
//{
//		- InOutStream!RemoteCall control_;
//		- OutputStream!Message[size_t] outputs;
//		- InputStream!Object input_;
//		- size_t id_;
//			 + ProducerImpl id(size_t id);
//			 + size_t id();
//			+ ProducerImpl control(InOutStream!RemoteCall control);
//			+ InOutStream!RemoteCall control();
//			+ bool closed();
//			+ void close();
//			+ ProducerImpl input(InputStream!Object input);
//			+ InputStream!Object input();
//		+ ProducerImpl set(size_t key, OutputStream!Message output);
//		+ ProducerImpl remove(size_t key);
//		+ void perform();
//}
//class ConsumerImpl : Consumer!()
//{
//		- InOutStream!RemoteCall control_;
//		- InputStream!Message[size_t] inputs;
//		- OutputStream!Object output_;
//		- size_t id_;
//			 + ConsumerImpl id(size_t id);
//			 + size_t id();
//			+ ConsumerImpl control(InOutStream!RemoteCall control);
//			+ InOutStream!RemoteCall control();
//			+ ConsumerImpl output(OutputStream!Object output);
//			+ OutputStream!Object output();
//			+ bool closed();
//		+ void close();
//		+ ConsumerImpl set(size_t key, InputStream!Message input);
//		+ ConsumerImpl remove(size_t key);
//		+ void perform();
//}
//class ClientDistributor : Distributor!()
//{
//		- InOutStream!RemoteCall producer_;
//		- InOutStream!RemoteCall consumer_;
//		- InOutStream!RemoteCall control_;
//		- bool closed_;
//		- size_t id_;
//			 + ClientDistributor id(size_t id);
//			 + size_t id();
//			+ ClientDistributor producer(InOutStream!RemoteCall producer);
//			+ InOutStream!RemoteCall producer();
//			+ ClientDistributor consumer(InOutStream!RemoteCall consumer);
//			+ InOutStream!RemoteCall consumer();
//			+ ClientDistributor control(InOutStream!RemoteCall control);
//			+ InOutStream!RemoteCall control();
//			 + bool closed();
//			+ void close();
//		+ ClientDistributor set(size_t key, InputStream!Message input);
//		+ ClientDistributor remove(size_t key);
//		+ ClientDistributor set(size_t key, OutputStream!Message output);
//		+ void perform();
//}
//// D import file generated from 'src/aermicioi/broker/communication.d'
//interface Producer(T = Message, Z : OutputStream!T = OutputStream!T, Key = size_t) : Storage!(Z, Key), Performable, Closeable, Identifiable!size_t
//{
//}
//interface Consumer(T = Message, Z : InputStream!T = InputStream!T, Key = size_t) : Storage!(Z, Key), Performable, Closeable, Identifiable!size_t
//{
//}
//interface Distributor(T = Message, Z = T, W : OutputStream!T = OutputStream!T, Y : InputStream!T = InputStream!T, Key = size_t) : Producer!(T, W, Key), Consumer!(Z, Y, Key)
//{
//}
//// D import file generated from 'src/aermicioi/broker/distributor.d'
//interface Distributor(T) : Storage!(OutputStream!T, size_t), Storage!(InputStream!T, size_t), ControlAware, Performable, Closeable, Identifiable!size_t, IoSchedulerAware
//{
//}
//abstract class MessageDistributor(T) : Distributor!T
//{
//		# InputStream!T[size_t] inputs;
//		# OutputStream!T[size_t] outputs;
//		# InOutStream!RemoteCall control_;
//		# IoScheduler scheduler_;
//		# bool closed_;
//		# size_t id_;
//			 + MessageDistributor!T id(size_t id)
//			 + size_t id()
//			 + bool closed()
//			+ override MessageDistributor control(InOutStream!RemoteCall broker)
//			+ InOutStream!RemoteCall control()
//			+ MessageDistributor scheduler(IoScheduler scheduler)
//			+ IoScheduler scheduler()
//		+ MessageDistributor set(size_t id, InputStream!T input)
//		+ MessageDistributor set(size_t id, OutputStream!T output)
//		+ MessageDistributor remove(size_t id)
//		+ void close()
//	#    MessageDistributor!T closed(bool closed)
//}
//class FanoutMessageDistributor(T) : MessageDistributor!T
//{
//	-  AnyConditional!size_t inputAvailableOrClosedConditional;
//		+ this()
//		+ override FanoutMessageDistributor!T set(size_t id, InputStream!T stream)
//		+ override FanoutMessageDistributor!T set(size_t id, OutputStream!T stream)
//		+ override FanoutMessageDistributor!T remove(size_t id)
//		+ override  FanoutMessageDistributor!T control(InOutStream!RemoteCall broker)
//		+ void perform()
//}
//class RoundRobinDistributor(T) : MessageDistributor!T
//{
//	-  typeof(outputs.byKeyValue) byKeyValue;
//	+  void perform()
//}
//class BufferedDistributor(T) : MessageDistributor!T
//{
//}
//// D import file generated from 'src/aermicioi/broker/remote_call.d'
//interface RemoteCall : Transformable
//{
//	+ void execute(Object);
//}
//interface ClientIdInjectable(Key)
//{
//		+ ClientIdInjectable!Key client(Key id);
//		+ Key client();
//}
//abstract class TypedRemoteCall(T) if (allSatisfy!(templateAnd!(isClass, isInterface))) : RemoteCall
//{
//	+ final override void execute(Object obj)
//	+ void handleCastMismatch(Object obj)
//	+ void execute(T);
//}
//class DelegateBasedRemoteCall(alias Dg, T, Args...) if (isSomeFunction!Dg && arity!Dg == Args.length + 1 && is(Parameters!Dg[0] : T)) : TypedRemoteCall!T
//{
//		-  Tuple!Args data_;
//		+ this()
//		+ this(Args args)
//		 + DelegateBasedRemoteCall!(Dg, T, Args) setArgs(Args args)
//		 + Tuple!Args getArgs()
//		+ override void execute(T obj)
//		+ void serialize(Serializator serializator)
//		+ void unserialize(Unserializator unserializator)
//}
//class Message : TypedRemoteCall!(Consumer!())
//{
//		- size_t distributor_;
//		- ubyte[] payload_;
//			+ Message payload(ubyte[] payload);
//			+ ubyte[] payload();
//			+ Message distributor(size_t distributor);
//			+ size_t distributor();
//		+ override void execute(Consumer!());
//		+ void serialize(Serializator serializator);
//		+ void unserialize(Unserializator unserializator);
//}
//class AddInputRange : TypedRemoteCall!(Storage!(InputStream!Message, size_t))
//{
//		- InputStream!Message range_;
//		- size_t id_;
//		+ this();
//		+ this(size_t from, InputStream!Message range);
//			+ AddInputRange range(InputStream!Message range);
//			+ InputStream!Message range();
//			+ AddInputRange id(size_t id);
//			+ size_t id();
//		+ override void execute(Storage!(InputStream!Message, size_t) obj);
//		+ void serialize(Serializator serializator);
//		+ void unserialize(Unserializator unserializator);
//}
//class AddOutputRange : TypedRemoteCall!(Storage!(OutputStream!Message, size_t))
//{
//		- OutputStream!Message range_;
//		- size_t id_;
//		+ this();
//		+ this(size_t to, OutputStream!Message range);
//			+ AddOutputRange range(OutputStream!Message range);
//			+ OutputStream!Message range();
//			+ AddOutputRange id(size_t id);
//			+ size_t id();
//		+ override void execute(Storage!(OutputStream!Message, size_t) obj);
//		+ void serialize(Serializator serializator);
//		+ void unserialize(Unserializator unserializator);
//}
//class Publish : TypedRemoteCall!Broker, ClientIdInjectable!size_t
//{
//		- size_t client_;
//		- size_t to_;
//			 + Publish client(size_t client);
//			 + size_t client();
//			+ Publish to(size_t to);
//			+ size_t to();
//		+ override void execute(Broker obj);
//		+ void serialize(Serializator serializator);
//		+ void unserialize(Unserializator unserializator);
//}
//class Subscribe : TypedRemoteCall!Broker, ClientIdInjectable!size_t
//{
//		- size_t client_;
//		- size_t to_;
//			 + Subscribe client(size_t client);
//			 + size_t client();
//			+ Subscribe to(size_t to);
//			+ size_t to();
//		+ override void execute(Broker obj);
//		+ void serialize(Serializator serializator);
//		+ void unserialize(Unserializator unserializator);
//}
//class Serve : TypedRemoteCall!Broker
//{
//		- size_t id_;
//		- InOutStream!RemoteCall range_;
//		+ this();
//		+ this(size_t id, InOutStream!RemoteCall range);
//			+ Serve id(size_t id);
//			+ size_t id();
//			+ Serve range(InOutStream!RemoteCall range);
//			+ InOutStream!RemoteCall range();
//		+ override void execute(Broker obj);
//		+ void serialize(Serializator serializator);
//		+ void unserialize(Unserializator unserializator);
//}
//abstract class Declare : TypedRemoteCall!Broker
//{
//	-  size_t id_;
//		+ this();
//		+ this(size_t id);
//			+ Declare id(size_t id);
//			+ size_t id();
//		+ void serialize(Serializator serializator);
//		+ void unserialize(Unserializator unserializator);
//}
//class FanoutDeclare : Declare
//{
//		+ this();
//		+ this(size_t to);
//		+ override void execute(Broker obj);
//}
//class RoundRobinDeclare : Declare
//{
//		+ this();
//		+ this(size_t to);
//		+ override void execute(Broker obj);
//}
//class Close : TypedRemoteCall!Closeable
//{
//		+ override void execute(Closeable obj);
//		+ void serialize(Serializator serializator);
//		+ void unserialize(Unserializator unserializator);
//}
//class Remove(Type, Key) : TypedRemoteCall!(Storage!(Type, Key))
//{
//	-  Key id_;
//			 + Remove!(Type, Key) id(Key id)
//			 + Key id()
//		+ this(Key id)
//		+ override void execute(Storage!(Type, Key) storage)
//		+ void serialize(Serializator serializator)
//		+ void unserialize(Unserializator unserializator)
//}
//class RemoveClient : TypedRemoteCall!Broker, ClientIdInjectable!size_t
//{
//	-  size_t client_;
//			 + RemoveClient client(size_t client);
//			 + size_t client();
//		+ void execute(Broker broker);
//		+ void serialize(Serializator serializator);
//		+ void unserialize(Unserializator unserializator);
//}
//// D import file generated from 'src/aermicioi/broker/runner.d'
//interface Runner : IoSchedulerAware
//{
//	+ void run();
//	 + Runner scheduler(IoScheduler);
//}
//interface IoSchedulerAware
//{
//	 + IoSchedulerAware scheduler(IoScheduler);
//}
//interface Identifiable(T)
//{
//		+ Identifiable id(T id);
//		+ T id();
//}
//interface Performable
//{
//	+ void perform();
//}
//interface PerformableRunner : Runner
//{
//	 + PerformableRunner performable(Performable);
//}
//interface CloseablePerformableRunner : PerformableRunner
//{
//	 + CloseablePerformableRunner closeable(CloseAware);
//}
//class PerformableRunnerImpl : PerformableRunner
//{
//		- Performable performable_;
//		- IoScheduler scheduler_;
//			+ PerformableRunnerImpl performable(Performable performable);
//			+ Performable performable();
//			+ PerformableRunner scheduler(IoScheduler scheduler);
//			+ IoScheduler scheduler();
//		+ void run();
//}
//class CloseablePerformableRunnerImpl : PerformableRunnerImpl, CloseablePerformableRunner
//{
//	-  CloseAware closeable_;
//			+ CloseablePerformableRunnerImpl closeable(CloseAware closeable);
//			+ CloseAware closeable();
//		+ override void run();
//}
//// D import file generated from 'src/aermicioi/broker/scheduler.d'
//interface Conditional
//{
//	+ bool evaluate();
//}
//interface AggregatableConditional(Key) : Conditional, Storage!(Conditional, Key)
//{
//}
//class DummyCondition : Conditional
//{
//	+  bool evaluate();
//}
//class AnyConditional(Key) : AggregatableConditional!Key
//{
//	-  Conditional[Key] conditions;
//		+ AnyConditional!Key set(Key key, Conditional condition)
//		+ AnyConditional!Key remove(Key key)
//		+ bool evaluate()
//}
//class AllConditional(Key) : AggregatableConditional!Key
//{
//	-  Conditional[Key] conditions;
//		+ AllConditional!Key set(Key key, Conditional condition)
//		+ AllConditional!Key remove(Key key)
//		+ void evaluate()
//}
//class ConditionalFiber : Fiber
//{
//		- ThreadInfo threadInfo_;
//		- Conditional condition_;
//		+ this(void delegate() op, Conditional condition, ThreadInfo info);
//		 + ConditionalFiber condition(Conditional condition);
//		 + Conditional condition();
//		  + ConditionalFiber threadInfo(ThreadInfo threadInfo);
//		 + ref  ThreadInfo threadInfo();
//}
//interface IoScheduler : Scheduler
//{
//		+ void condition(Conditional);
//		+ void condition();
//}
//class IoFiberScheduler : IoScheduler
//{
//		- ConditionalFiber[] fibers;
//		- Conditional dummy;
//		- size_t current;
//		+ this();
//		+ void start(void delegate() op);
//		+ void spawn(void delegate() op);
//		 + void yield();
//		  + ref ThreadInfo thisInfo();
//		+ void condition(Conditional condition);
//		+ void condition();
//		 + Condition newCondition(Mutex m);
//		# void run();
//		# void append(void delegate() op);
//}
//// D import file generated from 'src/aermicioi/broker/server.d'
//interface Server : Runner
//{
//	 + Server control(InOutStream!RemoteCall broker);
//	+ Server listen(string, ushort);
//}
//class ServerImpl : SocketWatcherImpl, Server
//{
//		- InOutStream!RemoteCall control_;
//		- IoScheduler scheduler_;
//		- DuplexChannelFactory!RemoteCall duplexChannelFactory_;
//		- string address_;
//		- ushort port_;
//		- int backlog_;
//		- ObjectMarshaller marshaller_;
//			+ ServerImpl control(InOutStream!RemoteCall broker);
//			+ InOutStream!RemoteCall control();
//			+ ServerImpl address(string address);
//			+ string address();
//			+ ServerImpl port(ushort port);
//			+ ushort port();
//			+ ServerImpl backlog(int backlog);
//			+ int backlog();
//			+ override ServerImpl scheduler(IoScheduler scheduler);
//			+ override IoScheduler scheduler();
//			+ ServerImpl duplexChannelFactory(DuplexChannelFactory!RemoteCall duplexChannelFactory);
//			+ DuplexChannelFactory!RemoteCall duplexChannelFactory();
//			+ ServerImpl marshaller(ObjectMarshaller marshaller);
//			+ ObjectMarshaller marshaller();
//		+ Server listen(string address, ushort port);
//		+ override void run();
//		- ClientDistributor spawnClient(SocketStream conn);
//		- Producer!() factoryProducer(SocketStream conn, InOutStream!RemoteCall control);
//		- Consumer!() factoryConsumer(SocketStream conn, InOutStream!RemoteCall control);
//}
//// D import file generated from 'src/aermicioi/broker/socket.d'
//class SocketStream : Socket, InputStream!(ubyte[]), OutputStream!(ubyte[]), IoSchedulerAware
//{
//		- ubyte[2048] buffer;
//		- ubyte[] actual;
//		- bool writeable_;
//		- bool readable_;
//		- bool closed_;
//		- IoScheduler scheduler_;
//		- NotFullConditional notFullConditional;
//		- NotEmptyConditional notEmptyConditional;
//		- ubyte[] delegate() frontDg;
//	#     this();
//		+ this(AddressFamily fam, SocketType sockType, ProtocolType proto);
//		+ override  Socket accepting();
//		+ void open();
//		+ override  void close();
//			   + SocketStream scheduler(IoScheduler scheduler);
//			   + IoScheduler scheduler();
//				+ ubyte[] front();
//				+ bool empty();
//			+ ubyte[] moveFront();
//			+ void popFront();
//			+ void put(ubyte u);
//			+ void put(ubyte[] r);
//			+ int opApply(int delegate(ubyte[]) dg);
//			+ int opApply(int delegate(size_t, ubyte[]) dg);
//			   + SocketStream writeable(bool writeable);
//			   + bool writeable();
//			   + SocketStream readable(bool readable);
//			 + bool readable();
//			+ bool full();
//			+ bool closed();
//			+ bool opened();
//			- ubyte[] popableFront();
//			- ubyte[] normalFront();
//		   - SocketStream closed(bool closed);
//}
//class SocketJoiner : InputStream!ubyte
//{
//		- ubyte[] buff;
//		- InputStream!(ubyte[]) range_;
//		- ubyte delegate() frontDg;
//		+ this();
//			+ ubyte front();
//			+ bool empty();
//			+ SocketJoiner range(InputStream!(ubyte[]) range);
//			+ InputStream!(ubyte[]) range();
//			+ bool closed();
//			+ bool opened();
//		+ void close();
//		+ void open();
//		+ ubyte moveFront();
//		+ void popFront();
//		+ int opApply(int delegate(ubyte) dg);
//		+ int opApply(int delegate(size_t, ubyte) dg);
//		- ubyte initFront();
//		- ubyte popableFront();
//		- ubyte normalFront();
//}
//// D import file generated from 'src/aermicioi/broker/socket_watcher.d'
//interface SocketWatcher : Runner, Storage!(SocketStream, size_t)
//{
//	+ void check();
//}
//class SocketWatcherImpl : SocketWatcher
//{
//		- SocketStream[size_t] sockets;
//		- SocketSet writeable;
//		- SocketSet readable;
//		- SocketSet errorable;
//		- IoScheduler scheduler_;
//		+ this();
//			+ SocketWatcherImpl scheduler(IoScheduler scheduler);
//			+ IoScheduler scheduler();
//		+ SocketWatcherImpl set(size_t key, SocketStream socket);
//		+ SocketWatcherImpl remove(size_t key);
//		+ void check();
//		+ void run();
//}
//// D import file generated from 'src/aermicioi/broker/stream.d'
//interface EmptyAware
//{
//	 + bool empty();
//}
//interface FullAware
//{
//	 + bool full();
//}
//interface CloseAware
//{
//	 + bool closed();
//}
//interface OpenAware
//{
//	 + bool opened();
//}
//interface Seekable
//{
//	+ void seek(size_t);
//}
//interface Openable : OpenAware
//{
//	+ void open();
//}
//interface Closeable : CloseAware
//{
//	+ void close();
//}
//interface Stream : Openable, Closeable
//{
//}
//interface InputStream(E) : InputRange!E, Stream, EmptyAware
//{
//}
//interface OutputStream(E) : OutputRange!E, Stream, FullAware
//{
//}
//interface InOutStream(T, Z = T) : InputStream!T, OutputStream!Z
//{
//}
//class InOutStreamImpl(T, Z = T) : InOutStream!(T, Z)
//{
//		- InputStream!T input_;
//		- OutputStream!Z output_;
//		+ this()
//		+ this(InputStream!T input, OutputStream!Z output)
//			+ T front()
//			+ bool empty()
//			+ bool full()
//			+ InOutStreamImpl!(T, Z) input(InputStream!T input)
//			+ InputStream!T input()
//			+ InOutStreamImpl!(T, Z) output(OutputStream!Z output)
//			+ OutputStream!Z output()
//			+ bool closed()
//			+ bool opened()
//		+ void close()
//		+ void open()
//		+ T moveFront()
//		+ void popFront()
//		+ int opApply(int delegate(T) dg)
//		+ int opApply(int delegate(size_t, T) dg)
//		+ void put(Z element)
//}
//class NotEmptyConditional : Conditional
//{
//	-  EmptyAware emptyable_;
//		   + this(EmptyAware emptyable);
//			   + NotEmptyConditional emptyable(EmptyAware emptyable);
//			   + EmptyAware emptyable();
//		+ bool evaluate();
//}
//class NotFullConditional : Conditional
//{
//	-  FullAware fullable_;
//		   + this(FullAware fullable);
//			   + NotFullConditional fullable(FullAware fullable);
//			   + FullAware fullable();
//		+ bool evaluate();
//}
//class ClosedConditional : Conditional
//{
//	-  CloseAware closeAware_;
//		+ this(CloseAware closeAware);
//			 + ClosedConditional closeAware(CloseAware closeAware);
//			 + CloseAware closeAware();
//		+ bool evaluate();
//}
//// D import file generated from 'src/aermicioi/broker/transformer.d'
//interface Serializator : FullAware
//{
//	+ Serializator put(ubyte);
//	+ Serializator put(byte);
//	+ Serializator put(ushort);
//	+ Serializator put(short);
//	+ Serializator put(uint);
//	+ Serializator put(int);
//	+ Serializator put(ulong);
//	+ Serializator put(long);
//	+ Serializator put(float);
//	+ Serializator put(double);
//	+ Serializator put(char);
//	+ Serializator put(wchar);
//	+ Serializator put(dchar);
//	+ Serializator put(string);
//	+ Serializator put(wstring);
//	+ Serializator put(dstring);
//	+ Serializator put(Serializable);
//}
//interface Unserializator : EmptyAware
//{
//	+ ubyte getUbyte();
//	+ byte getByte();
//	+ ushort getUshort();
//	+ short getShort();
//	+ uint getUint();
//	+ int getInt();
//	+ ulong getUlong();
//	+ long getLong();
//	+ float getFloat();
//	+ double getDouble();
//	+ char getChar();
//	+ wchar getWchar();
//	+ dchar getDchar();
//	+ string getString();
//	+ wstring getWstring();
//	+ dstring getDstring();
//	+ Unserializator getUnserializable(Unserializable);
//		+ Unserializator get(ref ubyte data);
//		+ Unserializator get(ref byte data);
//		+ Unserializator get(ref ushort data);
//		+ Unserializator get(ref short data);
//		+ Unserializator get(ref uint data);
//		+ Unserializator get(ref int data);
//		+ Unserializator get(ref ulong data);
//		+ Unserializator get(ref long data);
//		+ Unserializator get(ref float data);
//		+ Unserializator get(ref double data);
//		+ Unserializator get(ref char data);
//		+ Unserializator get(ref wchar data);
//		+ Unserializator get(ref dchar data);
//		+ Unserializator get(ref string data);
//		+ Unserializator get(ref wstring data);
//		+ Unserializator get(ref dstring data);
//}
//interface StreamSerializator(T) : Serializator, Closeable, FullAware
//{
//	+ StreamSerializator!T output(OutputStream!T);
//}
//interface StreamUnserializator(T) : Unserializator, Closeable, EmptyAware
//{
//	+ StreamUnserializator!T input(InputStream!T);
//}
//interface Serializable
//{
//	+ void serialize(Serializator);
//}
//interface Unserializable
//{
//	+ void unserialize(Unserializator);
//}
//interface Transformable : Serializable, Unserializable
//{
//}
//interface Transformer : Serializator, Unserializator
//{
//}
//interface StreamingTransformer(T, Z = T) : Transformer, StreamSerializator!T, StreamUnserializator!Z
//{
//}
//class BufferedNetworkBinaryTransformer : StreamingTransformer!(ubyte[])
//{
//		- OutputStream!(ubyte[]) output_;
//		- InputStream!(ubyte[]) input_;
//		- SocketJoiner joined;
//		- ubyte[32] buffer;
//			+ BufferedNetworkBinaryTransformer input(InputStream!(ubyte[]) input);
//			+ InputStream!(ubyte[]) input();
//			+ BufferedNetworkBinaryTransformer output(OutputStream!(ubyte[]) output);
//			+ OutputStream!(ubyte[]) output();
//			+ bool empty();
//			+ bool full();
//			+ bool closed();
//		+ void close();
//	+ BufferedNetworkBinaryTransformer put(ubyte data);
//	+ BufferedNetworkBinaryTransformer put(byte data);
//	+ BufferedNetworkBinaryTransformer put(ushort data);
//	+ BufferedNetworkBinaryTransformer put(short data);
//	+ BufferedNetworkBinaryTransformer put(uint data);
//	+ BufferedNetworkBinaryTransformer put(int data);
//	+ BufferedNetworkBinaryTransformer put(ulong data);
//	+ BufferedNetworkBinaryTransformer put(long data);
//	+ BufferedNetworkBinaryTransformer put(float data);
//	+ BufferedNetworkBinaryTransformer put(double data);
//	+ BufferedNetworkBinaryTransformer put(char data);
//	+ BufferedNetworkBinaryTransformer put(wchar data);
//	+ BufferedNetworkBinaryTransformer put(dchar data);
//	+ BufferedNetworkBinaryTransformer put(string data);
//	+ BufferedNetworkBinaryTransformer put(wstring data);
//	+ BufferedNetworkBinaryTransformer put(dstring data);
//	+ BufferedNetworkBinaryTransformer put(Serializable data);
//	+ override ubyte getUbyte();
//	+ override byte getByte();
//	+ override ushort getUshort();
//	+ override short getShort();
//	+ override uint getUint();
//	+ override int getInt();
//	+ override ulong getUlong();
//	+ override long getLong();
//	+ override float getFloat();
//	+ override double getDouble();
//	+ override char getChar();
//	+ override wchar getWchar();
//	+ override dchar getDchar();
//	+ override string getString();
//	+ override wstring getWstring();
//	+ override dstring getDstring();
//	+ override Unserializator getUnserializable(Unserializable data);
//}
//class TypeAwareTransformer(T, Z = T) : StreamingTransformer!(T, Z)
//{
//	-  StreamingTransformer!(T, Z) transformer_;
//		+ this()
//		+ this(StreamingTransformer!(T, Z) transformer)
//			+ Transformer transformer(StreamingTransformer!(T, Z) transformer)
//			+ StreamingTransformer!(T, Z) transformer()
//			+ bool empty()
//			+ bool full()
//			+ bool closed()
//			+ TypeAwareTransformer!(T, Z) input(InputStream!T input)
//			+ TypeAwareTransformer!(T, Z) output(OutputStream!Z output)
//		+ void close()
//		+ Serializator put(ubyte data)
//		+ Serializator put(byte data)
//		+ Serializator put(ushort data)
//		+ Serializator put(short data)
//		+ Serializator put(uint data)
//		+ Serializator put(int data)
//		+ Serializator put(ulong data)
//		+ Serializator put(long data)
//		+ Serializator put(float data)
//		+ Serializator put(double data)
//		+ Serializator put(char data)
//		+ Serializator put(wchar data)
//		+ Serializator put(dchar data)
//		+ Serializator put(string data)
//		+ Serializator put(wstring data)
//		+ Serializator put(dstring data)
//		+ Serializator put(Serializable data)
//		+ ubyte getUbyte()
//		+ byte getByte()
//		+ ushort getUshort()
//		+ short getShort()
//		+ uint getUint()
//		+ int getInt()
//		+ ulong getUlong()
//		+ long getLong()
//		+ float getFloat()
//		+ double getDouble()
//		+ char getChar()
//		+ wchar getWchar()
//		+ dchar getDchar()
//		+ string getString()
//		+ wstring getWstring()
//		+ dstring getDstring()
//		+ Unserializator getUnserializable(Unserializable data)
//}
//interface ObjectMarshaller
//{
//	+ void marshall(Object, Serializator);
//	+ Object unmarshall(Unserializator);
//}
//class ObjectMarshallerImpl : ObjectMarshaller
//{
//		+ void marshall(Object obj, Serializator transformer);
//		+ Object unmarshall(Unserializator transformer);
//}
//class ObjectMarshallerInputRange : InputStream!Object, CloseAware
//{
//		- StreamUnserializator!(ubyte[]) unserializator_;
//		- ObjectMarshaller marshaller_;
//		- Object current_;
//		- Object delegate() frontDg;
//			+ ObjectMarshallerInputRange unserializator(StreamUnserializator!(ubyte[]) unserializator);
//			+ StreamUnserializator!(ubyte[]) unserializator();
//			+ ObjectMarshallerInputRange marshaller(ObjectMarshaller marshaller);
//			+ ObjectMarshaller marshaller();
//			+ Object front();
//			+ bool closed();
//			+ bool empty();
//			+ bool opened();
//		+ void close();
//		+ void open();
//		+ void popFront();
//		+ Object moveFront();
//		+ int opApply(int delegate(Object) dg);
//		+ int opApply(int delegate(size_t, Object) dg);
//}
//class ObjectMarshallerOutputRange : OutputStream!Object, CloseAware
//{
//		- StreamSerializator!(ubyte[]) serializator_;
//		- ObjectMarshaller marshaller_;
//			+ ObjectMarshallerOutputRange serializator(StreamSerializator!(ubyte[]) serializator);
//			+ StreamSerializator!(ubyte[]) serializator();
//			+ ObjectMarshallerOutputRange marshaller(ObjectMarshaller marshaller);
//			+ ObjectMarshaller marshaller();
//			+ bool full();
//			+ bool closed();
//			+ bool opened();
//		+ void close();
//		+ void open();
//		+ void put(Object obj);
//}
//class TypeMismatchException : Exception
//{
//	   + this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable next = null);
//	   + this(string msg, Throwable next, string file = __FILE__, size_t line = __LINE__);
//}