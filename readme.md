Installation
============

##Dub package repository

Simply download the pre-compiled package for your platform. On Windows, the installer will perform all the necessary setup. On other systems, the only thing that you may want to do, other than extracting the archive, is to place the dub executable somewhere in your PATH so that you can call it from anywhere.
If a binary is not available for your platform, simply clone or download the dub repository and run the contained ./build.sh. You need to have DMD and libcurl development files installed (Only DMD on Windows).
Here is a link to download page [dub package manager](https://code.dlang.org/download).

## DMD compiler
Dowload and install dmd compiler.
[dmd compiler](https://dlang.org/download.html)

## Compilation process.
Run `dub build` from project root to build the application. Dub will fetch automatically all required dependencies and compile with one of available compilers.
An executable will be placed in root folder of application.

## Running application.
For testing purposes compiled application can run in 3 modes
    - server - run application as a broker. pass "server" as second argument.
    - producer - run application as a producer of messages. pass "producer" as second argument, and as third exchange node.
    - consumer - run application as a consumer of messages. pass "consumer" as second argument, and as third exchange node.

Example:
```sh
./pad_131_4_1 server
./pad_131_4_1 consumer 23
./pad_131_4_1 producer 23
```