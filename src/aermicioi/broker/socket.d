/**
License:
	Boost Software License - Version 1.0 - August 17th, 2003

	Permission is hereby granted, free of charge, to any person or organization
	obtaining a copy of the software and accompanying documentation covered by
	this license (the "Software") to use, reproduce, display, distribute,
	execute, and transmit the Software, and to prepare derivative works of the
	Software, and to permit third-parties to whom the Software is furnished to
	do so, all subject to the following:
	
	The copyright notices in the Software and this entire statement, including
	the above license grant, this restriction and the following disclaimer,
	must be included in all copies of the Software, in whole or in part, and
	all derivative works of the Software, unless such copies or derivative
	works are solely in the form of machine-executable object code generated by
	a source language processor.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
	SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
	FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
	ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.

Authors:
	aermicioi
**/
module aermicioi.broker.socket;

import std.range;
import std.socket;

import aermicioi.broker.stream;
import aermicioi.broker.runner;
import aermicioi.broker.scheduler;

class SocketStream : Socket, InputStream!(ubyte[]), OutputStream!(ubyte[]), IoSchedulerAware {
    
    private {
        ubyte[2048] buffer;
        ubyte[] actual;
        bool writeable_;
        bool readable_;
        bool closed_;
        IoScheduler scheduler_;
        NotFullConditional notFullConditional;
        NotEmptyConditional notEmptyConditional;
        
        ubyte[] delegate() frontDg; // This is good, we've got around of this inconsistency. 
    }
    
    protected {
        this() @trusted nothrow pure {
            notFullConditional = new NotFullConditional(this);
            notEmptyConditional = new NotEmptyConditional(this);
            frontDg = &this.popableFront;
            super();
        }
    }
    
    public {
        
        this(AddressFamily fam, SocketType sockType, ProtocolType proto) {
            super(fam, sockType, proto);
            frontDg = &this.popableFront;
        }
        
        override Socket accepting() nothrow {
            auto sock = new SocketStream;
            sock.scheduler = this.scheduler();
            
            return sock;
        }
        
        void open() {
            
        }
        
        override void close() nothrow {
            this.closed = true;
            this.writeable = false;
            this.readable = false;
            this.shutdown(SocketShutdown.BOTH);
        }
        
        @property {
            SocketStream scheduler(IoScheduler scheduler) pure nothrow @safe {
            	this.scheduler_ = scheduler;
            
            	return this;
            }
            
            IoScheduler scheduler() pure nothrow @safe {
            	return this.scheduler_;
            }
        }
        
        final {
            @property {
                ubyte[] front() {
                    return this.frontDg();
                }
                
                bool empty() {
                	return !this.readable() || this.closed();
                }
            }
            
            ubyte[] moveFront() {
                ubyte[] arr = actual.dup;
                this.popFront();
                
                return arr;
            }
            
            void popFront() {
                ptrdiff_t result;
                
                do {
                    result = this.receive(buffer);
                    
                    if (result == Socket.ERROR) {
                        if (!wouldHaveBlocked()) {
                            this.close;
                            throw new Exception(this.getErrorText());
                        } else {
                            this.readable = false;
                            
//                            scheduler.condition(this.notEmptyConditional);
                            byte[] buff = new byte[2000];
                            this.receive(buff[], SocketFlags.PEEK);
                            scheduler.yield();
//                            scheduler.condition;
                        }
                    } else {
                        actual = buffer[0 .. result];
                    }
                } while (result < 0);
                
                if (result == 0) {
                    this.close;
                    
                    throw new Exception("Connection closed");
                }
            }
            
            void put(ubyte u) {
                this.send((&u)[0 .. u.sizeof]);
                scheduler.yield();
            }
            
            void put(ubyte[] r) {
                ptrdiff_t sent;
                do {
                    sent = this.send(r);
                    
                    if (sent == Socket.ERROR) {
                        if (!wouldHaveBlocked()) {
                            this.close;
                            throw new Exception(this.getErrorText());
                        } else {
                            this.writeable = false;
//                            scheduler.condition(notFullConditional);
                            scheduler.yield();
//                            scheduler.condition;
                            continue;
                        }
                    }
                    
                    r = r[sent .. $];
                } while (!r.empty);
            }
            
            int opApply(int delegate(ubyte[]) dg) {
                int res;
                
                while (!res) {
                    res = dg(this.front);
                    this.popFront;
                }
                
                return res;
            }
            
            int opApply(int delegate(size_t, ubyte[]) dg) {
                int res;
                
                for (size_t index; !res; ++index) {
                    res = dg(index, this.front);
                    this.popFront;
                }
                
                return res;
            }
            
            SocketStream writeable(bool writeable) nothrow @safe @nogc {
            	this.writeable_ = writeable;
            
            	return this;
            }
            
            bool writeable() nothrow @safe @nogc {
            	return this.writeable_;
            }
            
            SocketStream readable(bool readable) nothrow @safe @nogc {
            	this.readable_ = readable;
            
            	return this;
            }
            
            bool readable() nothrow {
            	return this.readable_;
            }
            
            bool full() {
                return !this.writeable || this.closed;
            }
            
            bool closed() {
            	return this.closed_;
            }
            
            bool opened() {
                return !this.closed;
            }
        }
    }
    
    private {
        final {
            
            ubyte[] popableFront() {
                if (this.actual is null) {
                    this.popFront();
                }
                
                this.frontDg = &normalFront;
                return this.normalFront();
            }
            
            ubyte[] normalFront() {
                return this.actual;
            }
        }
        
        SocketStream closed(bool closed) nothrow @safe @nogc {
        	this.closed_ = closed;
        
        	return this;
        }
    }
}

class SocketJoiner : InputStream!(ubyte) {
    private {
        ubyte[] buff;
        InputStream!(ubyte[]) range_;
        
        ubyte delegate() frontDg;
    }
    
    public {
        this() {
            frontDg = &initFront;
        }
        
        @property {
            ubyte front() {
                return this.frontDg();
            }
            
            bool empty() {
                if (buff.empty) {
                    return this.range.empty;
                }
                
                return false;
            }
            
            SocketJoiner range(InputStream!(ubyte[]) range) {
            	this.range_ = range;
            
            	return this;
            }
            
            InputStream!(ubyte[]) range() {
            	return this.range_;
            }
            
            bool closed() {
            	return !this.empty && this.range.closed;
            }
            
            bool opened() {
            	return this.range.opened;
            }
        }
        
        void close() {
            return this.range.close;
        }
        
        void open() {
            return this.range.open;
        }
    
        ubyte moveFront() {
            ubyte curr = this.front();
            this.popFront();
            
            return curr;
        }
    
        void popFront() {
            import std.array;
            buff = buff[1 .. $];
            
            if (buff.empty) {
                this.frontDg = &popableFront;
            }
        }
    
        int opApply(int delegate(ubyte) dg) {
            int result = 0;
            
            do {
                result = dg(this.front);
                this.popFront;
            } while (!result);
            
            return result;
        }
    
        int opApply(int delegate(size_t, ubyte) dg) {
            int result = 0;
            int inc = 0;
            do {
                result = dg(inc++, this.front);
                this.popFront;
            } while (!result);
            
            return result;
        }
    }
    
    private final {
        ubyte initFront() {
            
            if (this.buff is null) {
                this.buff = range.front;
            }
            this.frontDg = &normalFront;
            return this.normalFront();
        }
        
        ubyte popableFront() {
            
            if (this.buff.empty) {
                this.range.popFront;
                this.buff = this.range.front;
            }
            
            this.frontDg = &normalFront;
            
            return this.normalFront();
        }
        
        ubyte normalFront() {
            return this.buff[0];
        }
    }
}

SocketJoiner socketJoiner(InputStream!(ubyte[]) range) {
    auto joiner = new SocketJoiner;
    joiner.range = range;
    
    return joiner;
}