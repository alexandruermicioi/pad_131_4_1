/**
License:
	Boost Software License - Version 1.0 - August 17th, 2003

	Permission is hereby granted, free of charge, to any person or organization
	obtaining a copy of the software and accompanying documentation covered by
	this license (the "Software") to use, reproduce, display, distribute,
	execute, and transmit the Software, and to prepare derivative works of the
	Software, and to permit third-parties to whom the Software is furnished to
	do so, all subject to the following:
	
	The copyright notices in the Software and this entire statement, including
	the above license grant, this restriction and the following disclaimer,
	must be included in all copies of the Software, in whole or in part, and
	all derivative works of the Software, unless such copies or derivative
	works are solely in the form of machine-executable object code generated by
	a source language processor.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
	SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
	FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
	ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.

Authors:
	aermicioi
**/
module aermicioi.broker.distributor;

import aermicioi.broker.remote_call;
import aermicioi.broker.stream;
import aermicioi.broker.runner;
import aermicioi.broker.broker;
import aermicioi.broker.scheduler;
import aermicioi.aedi.storage.storage;

interface Distributor(T) : 
    Storage!(OutputStream!T, size_t), 
    Storage!(InputStream!T, size_t), 
    ControlAware,
    Performable,
    Closeable,
    Identifiable!size_t,
    IoSchedulerAware {
        
}

abstract class MessageDistributor(T) : Distributor!T {
    
    protected {
        InputStream!(T)[size_t] inputs;
        OutputStream!(T)[size_t] outputs;
        
        InOutStream!RemoteCall control_;
        IoScheduler scheduler_;
        bool closed_;
        size_t id_;
    }
    
    public {
        @property {
            MessageDistributor!T id(size_t id) @safe {
            	this.id_ = id;
            
            	return this;
            }
            
            size_t id() @safe {
            	return this.id_;
            }
            
            bool closed() @safe {
            	return this.closed_;
            }
            
            override MessageDistributor control(InOutStream!RemoteCall broker) {
            	this.control_ = broker;
            
            	return this;
            }
            
            InOutStream!RemoteCall control() {
            	return this.control_;
            }
            
            MessageDistributor scheduler(IoScheduler scheduler) {
            	this.scheduler_ = scheduler;
            
            	return this;
            }
            
            IoScheduler scheduler() {
            	return this.scheduler_;
            }
        }
        
        MessageDistributor set(size_t id, InputStream!T input) {
            this.inputs[id] = input;
            
            return this;
        }
        
        MessageDistributor set(size_t id, OutputStream!T output) {
            this.outputs[id] = output;
            
            return this;
        }
        
        MessageDistributor remove(size_t id) {
            if (id in this.inputs) {
                this.inputs[id].close;
                this.inputs.remove(id);
            }
            
            if (id in this.outputs) {
                this.outputs[id].close;
                this.outputs.remove(id);
            }
            
            return this;
        }
        
        void close() {
            this.control.close;
            
            foreach (output; outputs) {
                output.close;
            }
            
            foreach (input; inputs) {
                input.close;
            }
            
        }
    }
    
    protected {
        @property {
            MessageDistributor!T closed(bool closed) @safe {
            	this.closed_ = closed;
            
            	return this;
            }
        }
    }
}

class FanoutMessageDistributor(T) : MessageDistributor!T {
    
    private {
        AnyConditional!size_t inputAvailableOrClosedConditional;
    }
    
    public {
        this() {
            this.inputAvailableOrClosedConditional = new AnyConditional!size_t;
        }
        
        override FanoutMessageDistributor!T set(size_t id, InputStream!T stream) {
            super.set(id, stream);
            
            AnyConditional!size_t closedOrAvailableInputConditional = new AnyConditional!size_t;
            closedOrAvailableInputConditional.set(0, new ClosedConditional(stream));
            closedOrAvailableInputConditional.set(1, new NotEmptyConditional(stream));
            
            this.inputAvailableOrClosedConditional.set(id, closedOrAvailableInputConditional);
            
            return this;
        }
        
        override FanoutMessageDistributor!T set(size_t id, OutputStream!T stream) {
            super.set(id, stream);
            
            return this;
        }
        
        override FanoutMessageDistributor!T remove(size_t id) {
            super.remove(id);
            
            this.inputAvailableOrClosedConditional.remove(id);
            return this;
        }
        
        alias control = MessageDistributor!T.control;
        
        @property override FanoutMessageDistributor!T control(InOutStream!RemoteCall broker) {
        	super.control(broker);
        
            AnyConditional!size_t closedOrAvailableInputConditional = new AnyConditional!size_t;
            closedOrAvailableInputConditional.set(0, new ClosedConditional(broker));
            closedOrAvailableInputConditional.set(1, new NotEmptyConditional(broker));
            
            this.inputAvailableOrClosedConditional.set(size_t.max, closedOrAvailableInputConditional);
        	return this;
        }
        
        void perform() {
            scheduler.condition;
            
            import std.array;
            auto removableInputs = appender!(size_t[]);
            auto removableOutputs = appender!(size_t[]);
            removableInputs.reserve(inputs.length);
            removableOutputs.reserve(outputs.length);
            
            foreach (id, input; inputs) {
                
                if (input.closed) {
                    removableInputs ~= id;
                    continue;
                }
                
                if (!input.empty) {
                    input.popFront;

                    foreach (id, output; outputs) {
                        
                        if (output.closed) {
                            removableOutputs ~= id;
                            continue;
                        } else {
                            try {
                                output.put(input.front);
                            } catch (Exception e) {
                                
                            }
                        }
                    }
                }
            }
            
            foreach (id; removableInputs.data) {
                this.remove(id);
            }
            
            foreach (id; removableOutputs.data) {
                this.remove(id);
            }
            
            while (!this.control.empty) {
                this.control.popFront;
                this.control.front.execute(this);
            }
            
            if (inputs.length > 0) {
                this.scheduler.condition(this.inputAvailableOrClosedConditional);
            }
        }
    }
}

class RoundRobinDistributor(T) : MessageDistributor!T {
    
    private {
        typeof(outputs.byKeyValue) byKeyValue;
    }
    
    public {
        void perform() {
            foreach (id, input; inputs) {
                if (input.closed) {
                    this.remove(id);
                    continue;
                }            
                    
                if (!input.empty) {
                    input.popFront;
                    
                    while (true) {
                        if (byKeyValue.empty) {
                            byKeyValue = outputs.byKeyValue;
                        }
                        
                        auto output = byKeyValue.front.value;
                        auto identity = byKeyValue.front.key;
                        byKeyValue.popFront;
                        
                        if (output.closed) {
                            assert(0, "We should move it out of this scope");
//                            this.remove(identity);
//                            continue;
                        }
                        
                        if (!output.full) {
                            output.put(input.front);
                            break;
                        }
                        
                        while (!this.control.empty) {
                            this.control.popFront;
                            this.control.front.execute(this);
                        }
                    }
                }
            }
            
            while (!this.control.empty) {
                this.control.popFront;
                this.control.front.execute(this);
            }
        }
    }
}

class BufferedDistributor(T) : MessageDistributor!T {
    
}